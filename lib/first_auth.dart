import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_training/home.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FirstAuth extends StatefulWidget{
  @override
  _FirstAuthWidget createState() => _FirstAuthWidget();

}

class _FirstAuthWidget extends State<FirstAuth>{

  bool onHide = true;

  String _username, _password;

  final formKey = GlobalKey<FormState>();

  SharedPreferences pref;

  Future setCurrentUser(String username)async{
    pref = await SharedPreferences.getInstance();
    await pref.setString('username', username);
  }

  Future loadCurrentUser()async{
    pref = await SharedPreferences.getInstance();
    var result = await pref.getString('username');
    if(result!=null){
      Navigator.push(context, MaterialPageRoute(builder: (_)=>Home()));
    }
  }

  @override
  void initState() {
    super.initState();
    loadCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Icon(Icons.person, color: Colors.white,size: 160,),
            decoration: BoxDecoration(
              // color: Colors.blue,
                image: DecorationImage(
                    fit: BoxFit.fill,
                    // colorFilter: ColorFilter.mode(Colors.blue, BlendMode.color),
                    image: AssetImage("assets/image_default.png")
                ),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(90),
                    bottomRight: Radius.circular(90)
                )
            ),
          ),
          SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [

                //Form
                Form(
                  key: formKey,
                  child: Column(
                    children: [
                      //username
                      Container(
                        padding: EdgeInsets.all(24),
                        child: TextFormField(
                          validator: (input){
                            if(input.length>6){
                              print(input.length);
                              return "Username max 6 character";
                            }
                            return null;
                          },
                          onChanged: (input){
                            setState(() {
                              _username = input;
                            });
                          },
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.person),
                              labelText: "Username"
                          ),
                        ),
                      ),

                      Container(
                        padding: EdgeInsets.all(24),
                        child: TextFormField(
                          obscureText: onHide,
                          onChanged: (input){
                            setState(() {
                              _password = input;
                            });
                          },
                          decoration: InputDecoration(
                              labelText: "Password",
                              prefixIcon: Icon(Icons.vpn_key),
                              suffixIcon: IconButton(
                                onPressed: (){
                                  setState(() {
                                    onHide = !onHide;
                                  });
                                },
                                icon: Icon(Icons.visibility_off),
                              )
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                FlatButton(
                    onPressed: (){
                      if(formKey.currentState.validate()){
                        print("RESULTS");
                        print("Username : $_username, Password : $_password");
                        setCurrentUser(_username).whenComplete((){
                          Navigator.push(context, MaterialPageRoute(builder: (_)=>Home(name: _username,)));
                        });
                      }
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      color: Colors.blue,
                      margin: EdgeInsets.all(24),
                      child: Center(
                        child: Text("Login", style: TextStyle(color: Colors.white),),
                      ),
                    )
                )
              ],
            ),
          )
        ],
      )
    );
  }

}