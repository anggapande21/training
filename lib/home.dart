import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_training/data_helper.dart';
import 'package:flutter_training/first_auth.dart';
import 'package:flutter_training/model/news.dart';
import 'package:flutter_training/news_detail.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget{
  String name;

  Home({this.name});

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>{

  SharedPreferences pref;

  String username;

  Color color;

  int selectedId;

  List<News> newsList = DataHelper.getDataNews();

  Future loadCurrentUser()async{
    pref = await SharedPreferences.getInstance();
    var result = await pref.getString('username');
    setState(() {
      username = result;
    });
  }

  Future logoutCurrentUser()async{
    pref = await SharedPreferences.getInstance();
    await pref.remove('username');
  }

  @override
  void initState() {
    super.initState();
    print("Results Parshing");
    print(widget?.name??"GUEST");
    loadCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${username??'GUEST'}"),
      ),
      body: SingleChildScrollView(
        padding:EdgeInsets.all(24),
        child: Column(
          children: newsList.map((modelku) {
            return itemNews(modelku);
          }).toList(),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: GestureDetector(
        onTap: (){
          logoutCurrentUser().whenComplete(() {
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                builder: (_)=>FirstAuth()), (route) => false);
          });
        },
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30)),
              color: Colors.indigo
          ),
          height: 40,
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.symmetric(horizontal: 24),
          padding: EdgeInsets.all(10),
          child: Text("Logout",textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 14),),
        ),
      ),
    );
  }
  
  Widget itemNews(News news){
    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (_)=>NewsDetail(news: news,)));
      },
      onDoubleTap: (){
        setState(() {
          selectedId = news.id;
          color = Colors.blueAccent;
        });
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 16),
        decoration: BoxDecoration(
          color: selectedId == news.id ? color : Colors.white,
           boxShadow: [BoxShadow(
             color: Colors.black12,
             offset: Offset(5, 5),
             blurRadius: 4, 
             spreadRadius: 3
           )]   ,
          borderRadius: BorderRadius.all(Radius.circular(16)),
        ),
        padding: EdgeInsets.all(14),
        width: MediaQuery.of(context).size.width,
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(16)),
              child: Image.asset(news.image,width: 60, height: 60,),
            ),
            SizedBox(width: 16,),

            //penggunaan recomended dalam row atau column
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(16))
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(news.title, style: TextStyle(fontWeight: FontWeight.bold),),
                    SizedBox(height: 8,),
                    Text(news.content.length > 120 ? news.content.substring(0, 120) + " ..." : news.content,
                      textAlign: TextAlign.justify,)
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

}