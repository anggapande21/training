import 'package:flutter_training/model/cat.dart';

class CatResponse{

  List<Cat> listCat;

  CatResponse.fromJson(Map<String, dynamic> jsonMap){
    listCat = jsonMap['all'] !=null ?
      List.from(jsonMap['all']).map((element) => Cat.fromJson(element)).toList() : [];
  }

}