class News {
  int id;
  String title;
  String content;
  String image;

  News({this.id, this.title, this.content, this.image});
}