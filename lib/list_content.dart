import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_training/helper.dart';
import 'package:flutter_training/model/cat.dart';

class ListContent extends StatelessWidget{

  List<Cat> listCat;
  ListContent({this.listCat});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: listCat.map((e) {
          return Container(
            decoration: Helper.of(context).decoration(),
            padding: EdgeInsets.all(16),
            margin: EdgeInsets.only(bottom: 16),
            child: Text(e.text),
          );
        }).toList(),
      ),
    );
  }

}