import 'model/news.dart';

class DataHelper{

  static getDataNews(){
    List<News> list =  new List<News>();

    list.add(News(id: 1, title: "First Training Element Pelangi",
        content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      image: "assets/image_default.png"
    ));

    list.add(News(id: 2, title: "Element Pelangi",
      content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      image: "assets/dummy-avatar.png"
    ));

    list.add(News(id: 3, title: "First Third Element Pelangi First Second ",
      content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      image: "assets/image_default.png"
    ));

    return list;
  }

}