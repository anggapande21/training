import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_training/list_content.dart';
import 'package:flutter_training/model/catResponse.dart';
import 'package:flutter_training/model/news.dart';
import 'package:http/http.dart' as http;

import 'model/cat.dart';

class NewsDetail extends StatefulWidget{

  News news;
  NewsDetail({this.news});

  @override
  _NewsDetalState createState() => _NewsDetalState();

}

class _NewsDetalState extends State<NewsDetail>{

  List<Cat> listCat = new List<Cat>();

  bool onProgress = false;

  bool onVisible = false;

  Future<CatResponse> getAllCat() async{
    final String url = "https://cat-fact.herokuapp.com/facts/";
    
    final client = new http.Client();
    
    var response =  await client.get(url);

    // log(response.body);

    return CatResponse.fromJson(json.decode(response.body));
  }

  Future getCatList() async{
    CatResponse catResponse =  await getAllCat();

    if(catResponse.listCat.isNotEmpty){
      log("ADA DATA");
      setState(() {
        listCat.addAll(catResponse.listCat);
      });
    }else{
      log("TIDAK ADA DATA");
    }
  }

  @override
  void initState() {
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text("Detail News", style: TextStyle(color: Colors.black) ,),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(24),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 16),
              width: MediaQuery.of(context).size.width,
              child: Text(widget.news.title, style: TextStyle(fontWeight: FontWeight.w700),),
            ),
            Container(
              child: Text(widget.news.content, textAlign: TextAlign.justify,),
            ),

            onVisible == false ? Container() : onProgress ? Container(
              margin: EdgeInsets.only(top: 24),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ) : Column(
              children: listCat.map((e) {
                return Container(
                  padding: EdgeInsets.all(16),
                  margin: EdgeInsets.only(bottom: 16),
                  child: Text(e.text),
                );
              }).toList(),
            )
          ],
        ),
      ),
      bottomNavigationBar: FlatButton(
        color: Colors.blue,
        padding: EdgeInsets.all(16),
        child: Text("Check Data", style: TextStyle(color: Colors.white),),
        onPressed: (){
          setState(() {
            onProgress = true;
            onVisible = true;
          });
          getCatList().whenComplete(() {
            setState(() {
              onProgress = false;
            });
          });
        },
      ),
    );
  }

}